![GitHub last commit](https://img.shields.io/github/last-commit/google/skia.svg)

# First Flutter Tests

Following these tutorials:

1) https://flutter.io/get-started/codelab/

2) https://www.youtube.com/watch?v=LHZ0KSvTTqQ&list=PLTAtF_0PvkNRd1OBo4n4a7Ct3bJoV34np&index=2

To get a better understanding of Flutter

## Getting Started

Testing app to demo Flutter basics.


