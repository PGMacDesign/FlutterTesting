import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';

/// From: https://www.youtube.com/watch?v=-MCeWP3rgI0&list=PLTAtF_0PvkNRd1OBo4n4a7Ct3bJoV34np&index=8
/// Scoped model is the set of utilities pulled from the Fuscia core OS (upcoming, new OS) repository
class ManageScopedModelPattern extends StatelessWidget{
	
	@override
	Widget build(BuildContext context) {
		return MaterialApp(
			title: 'Counter Example',
			theme: ThemeData.dark(),
			//All widgets within have access to app model
			home: ScopedModel<MyScopeModeledPattern>(
				model: MyScopeModeledPattern(),
//				child: Home(), //Simpler version
				child: Home2(), //More complicated version
			),
		);
	}
}

class Home extends StatelessWidget {
	
	@override
	Widget build(BuildContext context) {
		return Scaffold(
			appBar: AppBar(
				title: Text("Basic Counter"),
			),
			body: Center(
				child: Column(
					mainAxisAlignment: MainAxisAlignment.center,
					children: <Widget>[
						Text("Counter:"),
						//Contains the app model inside of it. Tells enclosed widget to find closest widget of the app model
						ScopedModelDescendant<MyScopeModeledPattern>(
							builder: (context, child, model) => Text(
								model.count.toString(), //Calling the getter method in MyScopeModeledPattern
								style: Theme.of(context).textTheme.display1,
							),
						)
					],
				),
			),
			floatingActionButton: ScopedModelDescendant<MyScopeModeledPattern>(
				builder: (context, child, model) => ButtonBar(
					children: <Widget>[
						IconButton(
							icon: Icon(Icons.add),
							onPressed: () => model.increment(), //Call to void function
						),
						IconButton(
							icon: Icon(Icons.minimize),
							onPressed: () => model.decrement(), //Call to void function
						)
					],
				),
			),
		);
	}
}

class Home2 extends StatelessWidget {
	final MyScopeModeledPattern appModel1 = MyScopeModeledPattern();
	final MyScopeModeledPattern appModel2 = MyScopeModeledPattern();
	
	@override
	Widget build(BuildContext context) {
		return Scaffold(
			appBar: AppBar(
				title: Text("Basic Counter"),
			),
			body: Center(
				child: Row(
					mainAxisAlignment: MainAxisAlignment.spaceEvenly,
					children: <Widget>[
						ScopedModel<MyScopeModeledPattern>(
							model: appModel1,
							child: MyCounter(counterName: "App Model 1"),
						),
						ScopedModel<MyScopeModeledPattern>(
							model: appModel2,
							child: MyCounter(counterName: "App Model 2", key: key), //No clue wtf this key is for
						),
					],
				),
			),
		);
	}
}

class MyCounter extends StatelessWidget {
	final String counterName;
//	MyCounter(Key key, this.counterName){}
	MyCounter({Key key, this.counterName});
	
	@override
	Widget build(BuildContext context) {
		return ScopedModelDescendant<MyScopeModeledPattern>(
			builder: (context, child, model) => Column(
				mainAxisAlignment: MainAxisAlignment.center,
				children: <Widget>[
					Text("$counterName:"),
					Text(model.count.toString(), style: Theme.of(context).textTheme.display1),
					ButtonBar(children: <Widget>[
						IconButton(
							icon: Icon(Icons.add),
							onPressed: () => model.increment(), //Call to void function
						),
						IconButton(
							icon: Icon(Icons.minimize),
							onPressed: () => model.decrement(), //Call to void function
						)
					],)
				],
			),
		);
	}
	
	
}

class MyScopeModeledPattern extends Model {
	int _count = 0;
	int get count => _count; //Getter
	
	void increment(){
		_count++;
		notifyListeners(); //This will notify state tree model has changed (IE notifyDataSetChanged)
	}
	
	void decrement(){
		_count--;
		notifyListeners(); //This will notify state tree model has changed (IE notifyDataSetChanged)
	}
	
}
