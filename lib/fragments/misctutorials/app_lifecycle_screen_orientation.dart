import 'package:flutter/material.dart';
import 'package:flutter/services.dart'; //Exposes services closer to the metal

/// This will primarily focus on the following life cycle events in each os:
/// Android: onPause and onResume
/// ApplicationDidEnterBackground (onPause) EnterForegroundMetho (onResume)
/// From: https://www.youtube.com/watch?v=s7pHLcQxP40&list=PLTAtF_0PvkNRd1OBo4n4a7Ct3bJoV34np&index=5
class AppLifecycleScreenOrientation extends StatelessWidget{
	
	@override
	Widget build(BuildContext context) {
		SystemChrome.setPreferredOrientations([
			DeviceOrientation.landscapeLeft, DeviceOrientation.portraitUp
		]);
		return MaterialApp(
			title: 'Orientation and Lifecycle',
			theme: ThemeData(
				primarySwatch: Colors.blue
			),
			home: LifecycleScreenOrientation(),
		);
	}
}

class LifecycleScreenOrientation extends StatefulWidget {
	@override
	MyHomeState createState() => MyHomeState();
}

//Mixing gives us access to lifecycle events via listeners / observers. (As well as didChangeLifecycleState
class MyHomeState extends State<LifecycleScreenOrientation> with WidgetsBindingObserver {
	
	AppLifecycleState appLifecycleState;
	List<String> _data;
	String _getData;
	
	/// AKA EditText
	TextEditingController controller = TextEditingController();
	
	@override
	Widget build(BuildContext context) {
		return Scaffold(
			appBar: AppBar(
				title: Text("Orientation and Lifecycle", textAlign: TextAlign.center),
			),
			body: OrientationBuilder(
				builder: (BuildContext context, Orientation orientation){
					return Center(
						child: Column(
							children: <Widget>[
								TextField(
									controller: controller,
								),
//								Text((_data.length == 0) ? "N/A" : _data[0])
								Text(_getData ?? "N/A")
							],
						),
					);
//					return Center(
//						child: GridView.count(
//							crossAxisCount: orientation == Orientation.landscape ? 4 : 2,
//							children: List.generate(40, (int i){
//								return Text("Tile $i");
//							}),
//						)
//					);
				},
			),
		);
	}
	
	
	@override
	void initState() {
		WidgetsBinding.instance.addObserver(this);
		_data = []; //Init to empty list
	}
	
	@override
	void dispose() {
		WidgetsBinding.instance.removeObserver(this);
	}
	
	@override
	void didChangeAppLifecycleState(AppLifecycleState state) {
		setState(() => appLifecycleState = state);
		print("state has changed to: $appLifecycleState");
		switch(state){
			case AppLifecycleState.inactive:
				
				break;
			
			case AppLifecycleState.paused:
				controller.text.isNotEmpty ? _data.add(controller.text) : null;
				controller.text = "";
				break;
		
			case AppLifecycleState.resumed:
				_getData = _data[0];
				_data = [];
				break;
			
			case AppLifecycleState.suspending:
			
				break;
		
		
		}
	}
	
	
}