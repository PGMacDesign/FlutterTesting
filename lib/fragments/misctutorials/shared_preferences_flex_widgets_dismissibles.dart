import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:async';

const String SHARED_PREFERENCES_LIST_KEY = "list_one";

class SharedPreferencesFlexWidgetsDismissibles extends StatelessWidget{
	
	@override
	Widget build(BuildContext context) {
		return MaterialApp(
			home: Scaffold(
				appBar: AppBar(
					title: Text('Flutter Shared_Prefs example'),
				),
				body: SharedPrefsHome(),
			)
		);
	}
}

class SharedPrefsHome extends StatefulWidget {
	@override
	SharedPrefsHomeState createState() => SharedPrefsHomeState();
}

class SharedPrefsHomeState extends State<SharedPrefsHome> {
	
	Future<SharedPreferences> _sPrefs = SharedPreferences.getInstance();
	final TextEditingController controller = TextEditingController();
	List<String> list_one, list_two;
	
	
	@override
	void initState() {
		super.initState();
		this.list_one = [];
		this.list_two = [];
	}
	
	/// To add items to the Shared Preferences
	Future<Null> addString() async {
		final SharedPreferences prefs = await this._sPrefs;
		this.list_one.add(this.controller.text); // Text in the input box
		prefs.setStringList(SHARED_PREFERENCES_LIST_KEY, this.list_one);
		setState((){
			this.controller.text = "";
		});
	}
	
	/// To clear list items from Shared Preferences
	Future<Null> clearItems() async {
		final SharedPreferences prefs = await this._sPrefs;
		prefs.remove(SHARED_PREFERENCES_LIST_KEY);
		setState(() {
			clearLists();
		});
	}
	
	/// To clear list items from Shared Preferences
	Future<Null> clearAllSPs() async {
		final SharedPreferences prefs = await this._sPrefs;
		prefs.clear();
		setState(() {
			clearLists();
		});
	}
	
	Future<Null> getStrings() async {
		final SharedPreferences prefs = await _sPrefs;
		this.list_two = prefs.getStringList(SHARED_PREFERENCES_LIST_KEY);
		/// Setting an empty set state is like calling notifyDataSetChanged()
		this.setState((){});
	}
	
	Future<Null> updateStrings(String str) async {
		final SharedPreferences prefs = await _sPrefs;
		setState(() {
			list_one.remove(str);
			list_two.remove(str);
		});
		prefs.setStringList(SHARED_PREFERENCES_LIST_KEY, list_one);
		
	}
	
	/// Clear lists
	void clearLists(){
		this.list_one = [];
		this.list_two = [];
	}
	
	@override
	Widget build(BuildContext context) {
		// do stuff here
		getStrings();
		return Center(
			child: ListView(
				children: <Widget>[
					TextField(
						controller: controller,
						decoration: InputDecoration(
							hintText: "Type in something..."
						),
					),
					RaisedButton(
						child: Text("Submit"),
						onPressed: () => addString(),
					),
					RaisedButton(
						child: Text("Clear"),
						onPressed: () => clearItems(),
					),
					/// Very similar to the expanded widget (takes up the rest of the space)
					Flex(
						direction: Axis.vertical,
						children: (list_two == null) ? [] :
//							list_two.map((String s) => Text(s)).toList() //Text appended //Wrapped in a simple text widget
							list_two.map((String s) => Dismissible(
								key: Key(s),
								onDismissed: (direction){
									updateStrings(s);
								},
								child: ListTile(
									title: Text(s),
								),
							)).toList() //wrapped into a dismissible
					)
				],
			),
		);
	}
	

}