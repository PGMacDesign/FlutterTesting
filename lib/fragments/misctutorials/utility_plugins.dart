import 'package:flutter/material.dart';

/// https://www.youtube.com/watch?v=hLKtx2HzqXc&index=12&list=PLTAtF_0PvkNRd1OBo4n4a7Ct3bJoV34np

class NameOfClassHere extends StatelessWidget{
	
	@override
	Widget build(BuildContext context) {
		return MaterialApp(
			title: 'Some title here',
			theme: ThemeData(
				primarySwatch: Colors.blue
			),
			home: SubNameOfAppHere(),
		);
	}
}

class SubNameOfAppHere extends StatefulWidget {
	@override
	AppStateNameHere createState() => AppStateNameHere();
}

class AppStateNameHere extends State<SubNameOfAppHere> {
	
	@override
	Widget build(BuildContext context) {
		// do stuff here
		return null;
	}
}