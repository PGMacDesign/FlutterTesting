import 'package:flutter/material.dart';
import 'package:flutter/physics.dart';
import 'dart:math';

import 'package:startup_namer/misc/Constants.dart';

/// From: https://www.youtube.com/watch?v=LHZ0KSvTTqQ&list=PLTAtF_0PvkNRd1OBo4n4a7Ct3bJoV34np&index=2
/// Physics logic for various purposes
class SpringBox extends StatelessWidget {
	
	
	@override
	Widget build(BuildContext context) {
		return MaterialApp(
			title: "Spring Box",
			theme: ThemeData(
				primaryColor: Colors.red
			),
			home: HomePage()
		);
	}
}

class HomePage extends StatelessWidget {
	
	@override
	Widget build(BuildContext context) {
		return Scaffold(
			body: Container(
				child: Padding(
					child: PhysicsBox(
						boxPosition: 0.5,
					),
					padding: EdgeInsets.only(
						left: 10.0,
						right: 10.0,
						top: 20.0,
						bottom: 20.0
					)
				)
			),
		);
	}
}

class PhysicsBox extends StatefulWidget {
	final boxPosition;
	
	PhysicsBox({
		this.boxPosition = 0.0
	});
	
	@override
	State createState() => BoxState();
	
	
}

class BoxState extends State<PhysicsBox> with TickerProviderStateMixin {
	double boxPosition;
	double boxPositionOnStart;
	Offset start;
	Offset point;
	
	AnimationController controller;
	ScrollSpringSimulation simulation;
	
	@override
	void initState() {
		super.initState();
		boxPosition = widget.boxPosition;
		this.simulation = ScrollSpringSimulation(
			SpringDescription(
				mass: 1.0,
				stiffness: 1.0,
				damping: 1.0
			),
			0.0,
			1.0,
			0.0
		);
		controller = AnimationController(vsync: this);
		controller.addListener((){
			print('${simulation.x(controller.value)}'); //Referenced @ 6:15 in https://www.youtube.com/watch?v=LHZ0KSvTTqQ&index=2&list=PLTAtF_0PvkNRd1OBo4n4a7Ct3bJoV34np
		});
	}
	
	@override
	Widget build(BuildContext context) {
		return GestureDetector(
			onPanStart : startDrag,
			onPanUpdate : onDrag,
			onPanEnd : endDrag,
			child: CustomPaint(
				painter: BoxPainter(
					color: Constants.BOX_COLOR,
					boxPosition: boxPosition,
					boxPositionOnStart: boxPositionOnStart ?? boxPosition, // From: https://stackoverflow.com/a/446839/2480714 , Null coalescing operator
					touchPoint: point
				),
				child: Container(),
			),
		);
	} //Holds logic for spring physics

	void startDrag(DragStartDetails details){
		//Take action on drag
		start = (context.findRenderObject() as RenderBox)
			.globalToLocal(details.globalPosition);
		boxPositionOnStart = boxPosition;
	}
	
	void onDrag(DragUpdateDetails details){
		setState(() {
			point = (context.findRenderObject() as RenderBox)
				.globalToLocal(details.globalPosition);
		});
		final dragVelocity = start.dy - point.dy;
		final normalDragVeloticy = (dragVelocity / context.size.height).clamp(-1.0, 1.0);
		this.boxPosition = (boxPosition + normalDragVeloticy).clamp(0.0, 1.0);
	}
	
	void endDrag(DragEndDetails details){
		setState(() {
			start = null;
			point = null;
			boxPositionOnStart = null;
		});
	}
	
	
}

class BoxPainter extends CustomPainter {
	final double boxPosition;
	final double boxPositionOnStart;
	final Color color;
	final Offset touchPoint;
	//Description for how to paint things on the canvas
	final Paint boxPaint;
	final Paint dropPaint;
	
	/// The deal with the weird constructor - https://stackoverflow.com/a/50274735/2480714
	BoxPainter({
		this.boxPosition = 0.0,
		this.boxPositionOnStart = 0.0,
		this.color = Colors.grey,
		this.touchPoint
	}) :
		boxPaint = Paint(),
		dropPaint = Paint() {
			boxPaint.color = this.color;
			boxPaint.style = PaintingStyle.fill;
			dropPaint.color = Colors.grey;
			dropPaint.style = PaintingStyle.fill;
	}
	
	@override
	void paint(Canvas canvas, Size size) {
		canvas.clipRect(Rect.fromLTWH(0.0, 0.0, size.width, size.height));
		final boxValueY = size.height - (size.height * boxPosition);
		final prevBoxValueY = size.height - (size.height * boxPositionOnStart);
		final midPointY = ((boxValueY - prevBoxValueY) * 1.2 + prevBoxValueY).clamp(0.0, size.height);
		
		Point left, mid, right;
		left = Point(-100.0, prevBoxValueY);
		right = Point(size.width + 50.0, prevBoxValueY);
		
		if(touchPoint != null){
			mid = Point(touchPoint.dx, midPointY);
		} else {
			mid = Point(size.width / 2, midPointY);
		}
		final path = Path(); //Adjusts concavity of path
		path.moveTo(mid.x, mid.y);
		path.quadraticBezierTo(mid.x - 100.0, mid.y, left.x, left.y);
		path.lineTo(0.0, size.height);
		path.moveTo(mid.x, mid.y);
		path.quadraticBezierTo(mid.x + 100, mid.y, right.x, right.y);
		path.lineTo(size.width, size.height);
		path.lineTo(0.0, size.height);
		path.close();
		
		canvas.drawPath(path, boxPaint);
		canvas.drawCircle(Offset(right.x, right.y), 10.0, dropPaint);
		
	}
	
	@override
	bool shouldRepaint(CustomPainter oldDelegate) {
		return true; //Always repaint no matter what
	}
	
	
}