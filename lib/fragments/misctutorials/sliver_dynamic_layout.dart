import 'package:flutter/material.dart';

/// Silvers are the Dynamic version of box rendered elements (Slice of a viewport)
/// They can have children (and nested). They generally have scroll physics built into them
/// They are typically lazy constructed, but this can be overloaded
/// From https://www.youtube.com/watch?v=wN2lpqxkB4M&list=PLTAtF_0PvkNRd1OBo4n4a7Ct3bJoV34np&index=4
class SliverDynamicLayout extends StatelessWidget {
	
	
	@override
	Widget build(BuildContext context) {
		return MaterialApp(
			title: "Sliver Demo",
			theme: ThemeData (
				primarySwatch: Colors.red
			),
			home: MySliverLayout(),
		);
	}
}

class MySliverLayout extends StatefulWidget {
	
	@override
	_MySliverLayoutState createState() => _MySliverLayoutState();
}

class _MySliverLayoutState extends State<MySliverLayout> {
	@override
	Widget build(BuildContext context) {
		return Scaffold(
			appBar: AppBar(
				title: Text("Sliver Example"),
			),
			body : CustomScrollView(
				slivers: <Widget>[
					SliverAppBar(
						title: Text("Sliver App Bar"),
						floating: true,
//						pinned: true, //Whether or not it moves with the scroll or not (Anchored or not. If stay when scrolling, pinned == true)
						expandedHeight: 250.0, //Allows height to change manually
						elevation: 10.0,
						flexibleSpace: FlexibleSpaceBar(
							background: Image.network(
								"https://images-na.ssl-images-amazon.com/images/I/81dbzOTS9cL._SX425_.jpg",
								fit: BoxFit.cover,
							),
							title: Text("Spacebar Title"),
						),
					),
					SliverGrid( //Like a listview / gridview, but 2 dimensions instead of one
						gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
							maxCrossAxisExtent: 400.0,
							mainAxisSpacing: 10.0,
							crossAxisSpacing: 10.0,
							childAspectRatio: 8.0
						),
						delegate: SliverChildBuilderDelegate(
								(BuildContext context, int index) {
							return Container (
								alignment: Alignment.center,
								color: Colors.purple[100 * (index % 9)],
								child: Text('Grid Item: $index'),
							);
							},
							childCount: 500
						),
					),
					/// These take up the full screen with the viewport fraction where 1.0 == the full screen
					SliverFillViewport(
						viewportFraction: 0.45, // Takes up 45% of the screen
						delegate: SliverChildBuilderDelegate((BuildContext context, int index){
							return Container(
								child: Text("Sliver fill viewport"),
								color: Colors.lightBlue,
							);
						},
							childCount: 2
						),
					),
					SliverFixedExtentList(
						itemExtent: 40.0, // 40 pixels from bottom to top
//						delegate: SliverChildListDelegate([
//							Text('111'),
//							Text('222'),
//							Text('333'),
//							Text('444')
//						]),
						delegate: SliverChildBuilderDelegate((BuildContext context, int index){
							return Container(
								alignment: Alignment.center,
								color: Colors.indigo[100* (index % 9)],
								child: Text('List Item: $index')
							);
						}, childCount: 10), //Without child count this will be infinite
					),
					SliverGrid( //Like a listview / gridview, but 2 dimensions instead of one
						gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
							crossAxisCount: 12,
							mainAxisSpacing: 10.0,
							crossAxisSpacing: 10.0,
							childAspectRatio: 8.0
						),
						delegate: SliverChildBuilderDelegate(
								(BuildContext context, int index) {
								return Container (
									alignment: Alignment.center,
									color: Colors.purple[100 * (index% 9)],
									child: Text('Grid Item: $index'),
								);
							},
							childCount: 25
						),
					)
				],
			)
		);
	}
}