import 'package:flutter/material.dart';
/// From: https://www.youtube.com/watch?v=On2Oew0NdGo&list=PLTAtF_0PvkNRd1OBo4n4a7Ct3bJoV34np&index=6
/// Note that this is being launched from the main.dart file in a separate package for organizational purposes.
class DragAndDropSample extends StatelessWidget {
	@override
	Widget build(BuildContext context) {
		return MaterialApp(
			home: Scaffold(
				body: DragDrop(),
			),
		);
	}
}

class DragDrop extends StatefulWidget {
	@override
	DragDropState createState() => DragDropState();
}

class SimpleObject {
	String str;
	Color color;
	SimpleObject(this.str, this.color){}
}

class DragDropState extends State<DragDrop> {
	Color caughtColor = Colors.grey;
	
	@override
	Widget build(BuildContext context) {
		//Line up position elements as per need
		return Stack(
			children: <Widget>[
				DragBox(Offset(0.0, 0.0), "Box 1", Colors.lime),
				DragBox(Offset(200.0, 0.0), "Box 2", Colors.blue),
				DragBox(Offset(300.0, 0.0), "Box 3", Colors.red),
				Container(
					alignment: Alignment.center,
					child: DragTarget(
						//This function fires whenever a box drops into the area
						//onAccept: (SimpleObject simpleObject){
						onAccept: (Color color){
//							caughtColor = simpleObject.color;
							caughtColor = color;
							print("onAccept Triggered");
						},
						builder: (BuildContext context,
							//These lists represent the items that are accepted or rejected when they get dragged in
							List<dynamic> accepted,
							List<dynamic> rejected){
								return Container(
									width: 200.0,
									height: 200.0,
									alignment: Alignment.center,
									decoration: BoxDecoration(
										color: accepted.isEmpty ? caughtColor : Colors.grey.shade200
									),
									child: Container(
		//								child: Text("--> Drag box here <--"),
										child: Text("--> Drag box here <--"),
									),
								);
							}
					),
				)
			],
		);
	}
}

/// UI
class DragBox extends StatefulWidget {
	final Offset initPos;
	final String label;
	final Color itemColor;
	
	DragBox(this.initPos, this.label, this.itemColor){}
	
	@override
	State createState() => DragBoxState();
}

class DragBoxState extends State<DragBox> {
	Offset position = Offset(0.0, 0.0);
	
	@override
	void initState() {
		super.initState();
		this.position = widget.initPos;
	}
	///Since everything is going on stack, need positioning
	@override
	Widget build(BuildContext context) {
		return Positioned(
			left: position.dx,
			top: position.dy,
			child: Draggable(
				data: widget.itemColor,
				child: Container(
					width: 100.0,
					height: 100.0,
					color: widget.itemColor,
					child: Center(
						child: Text(
							widget.label,
							style: TextStyle(
								color: Colors.white,
								decoration: TextDecoration.none,
								fontSize: 20.0
							),
						),
					),
					
				),
				onDraggableCanceled: (velocity, offset){
					setState(() {
					    position = offset;
					});
				},
				/// What is looks like when it is being dragged around
				feedback:Container(
					width: 120.0,
					height: 120.0,
					color: widget.itemColor.withOpacity(0.5),
					child: Center(
					child: Text(
						widget.label,
						style: TextStyle(
						color: Colors.white,
						decoration: TextDecoration.none,
						fontSize: 17.0)
					))
			),
		));
	}
}