import 'package:flutter/material.dart';
import 'dart:math';

/// From: https://www.youtube.com/watch?v=4I68ilX0Y24&index=9&list=PLTAtF_0PvkNRd1OBo4n4a7Ct3bJoV34np
/// Shows how to use inherited widgets to link states across to various widgets and re-render specific things as opposed to the whole widget
class InheritedWidgetsAndGestureDetectors extends StatelessWidget{
	
	@override
	Widget build(BuildContext context) {
		return MaterialApp(
			title: 'Random Squares',
			theme: ThemeData(
				primarySwatch: Colors.blue
			),
			home: SubNameOfAppHere(),
		);
	}
}

class SubNameOfAppHere extends StatefulWidget {
	@override
	AppStateNameHere createState() => AppStateNameHere();
}

class AppStateNameHere extends State<SubNameOfAppHere> {
	
	final Random _random = Random();
	Color color = Colors.amber;
	
	Color get getColors => color;
	
	//Triggers upon button click
	void onTap(){
		setState(() {
		    color = Color.fromRGBO(this._random.nextInt(256),
			    this._random.nextInt(256), this._random.nextInt(256),
			    this._random.nextDouble());
		});
	}
	
	@override
	Widget build(BuildContext context) {
		// do stuff here
		return ColorState(
			color: this.color,
			onTap: onTap,
			child: BoxTree()
		);
	}
}

class ColorState extends InheritedWidget {
	final Color color;
	final Function onTap;
	
	ColorState({
		Key key,
		this.color,
		this.onTap,
		Widget child,
	}) : super(key: key, child: child);
	
	@override
	bool updateShouldNotify(ColorState oldWidget) {
		return color != oldWidget.color;
	}
	
	/// Needs to be static as it is not an instance method because inherited method is essentially a singleton
	static ColorState of (BuildContext context){
		return context.inheritFromWidgetOfExactType(ColorState);
	}
}

class BoxTree extends StatelessWidget{
	
	@override
	Widget build(BuildContext context) {
		return Scaffold(
			body: Center(
				child: Row(
					children: <Widget>[
						Box(),
						Box()
					],
				),
			),
		);
	}
}

class Box extends StatelessWidget {
	
	@override
	Widget build(BuildContext context) {
		final colorState = ColorState.of(context);
		return GestureDetector(
			onTap: colorState.onTap, //Note, does not need '()'
			onLongPress: () => print("on long press"),
			onHorizontalDragDown: (arbitraryVariableWhichMakesNoSense) => print('finger dragged down'),
			onHorizontalDragStart: (arbitraryVariableWhichMakesNoSense) => print('starting drag'),
			onHorizontalDragUpdate: (arbitraryVariableWhichMakesNoSense) => print('stil dragging'),
			onHorizontalDragEnd: (arbitraryVariableWhichMakesNoSense) => print('end drag'),
			onHorizontalDragCancel: () => print('cancelled drag'),
			onScaleStart: (arbitraryVariableWhichMakesNoSense) => print("start scaling"),
			onScaleUpdate: (arbitraryVariableWhichMakesNoSense) => print("still scaling"),
			onScaleEnd: (arbitraryVariableWhichMakesNoSense) => print("scaling ended"),
			child: Container(
				width: 100.0,
				height: 100.0,
				margin: EdgeInsets.only(left: 200.0),
				color: colorState.color
			),
		);
	}
}