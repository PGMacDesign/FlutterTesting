import 'package:flutter/material.dart';
import 'dart:math' as math; //As adds alias
import 'dart:async';
/// Scrolling physics works like a ballistic pendulum.
/// Layout builder widgets use the parent widgets to construct a widget tree (Glues together parent + children)
/// Layout Widgets (UI Elements) all have designated constraints with min and max sizes as opposed to defined size (x,y)
/// Layout builder itself delays building children until layout phase of rendering cycle is complete
/// Rendering: (rootisolate -- Thread) acts platform for next vSync event, when it happens, engine executes dart code,
/// which updates animation and rebuilds widget and then layout widgets + paint them into layers.
/// Then submits to engine and constructs tree of nodes about the UI and then passes box constraints down the line for sizing.
/// Upshot is painting occurs before all rendering is done

/// From: https://www.youtube.com/watch?v=f_aN4aN3Xdc&list=PLTAtF_0PvkNRd1OBo4n4a7Ct3bJoV34np&index=7
////Since we are using layout builder widget as scaffold, set to async
Future<Null> main() async => runApp(ScrollingPhysics());

class ScrollingPhysics extends StatelessWidget{
	
	
	@override
	Widget build(BuildContext context) {
		return MaterialApp(
			/*
			title: 'Some title here',
			theme: ThemeData(
				primarySwatch: Colors.blue
			),
			home: SubNameOfAppHere(),
			 */
			home: LayoutBuilder(
				builder: (BuildContext context, BoxConstraints constraints){
					return Material(
						child: Center(
							child: ConstrainedBox(
								constraints: BoxConstraints(
									//These are coming from Material Application
									maxWidth: 400.0,
									maxHeight: constraints.maxHeight //IE, matchParent
								),
								child: ListView.builder(
									//Remove this line to go back to normal scroll physics as this adds custom implementation
									physics: CustomScrollPhysics(),
									itemExtent: 250.0,
									itemBuilder: (BuildContext context, int index) => Container(
										padding: EdgeInsets.symmetric(vertical: 9.0),
										child: Material(
											elevation: 4.0,
											borderRadius: BorderRadius.circular(5.0),
											color: (index % 2 == 0) ? Colors.cyan : Colors.deepOrange,
											child: Center(
												child: Text("Box #" + index.toString()),
											),
										),
										
									)
								),
							),
						),
						color: Colors.black38	
					);
				},
			)
		);
	}
}

class CustomScrollPhysics extends ScrollPhysics {
	@override
	ScrollPhysics applyTo(ScrollPhysics ancestor) {
		return CustomScrollPhysics();
	}
	
	@override
	Simulation createBallisticSimulation(ScrollMetrics position, double velocity) {
//		return super.createBallisticSimulation(position, velocity); //Normal
		return CustomPhysicsSimulation(position.pixels, velocity);
	}
}

class CustomPhysicsSimulation extends Simulation {
	final double initPosition;
	final double velocity;
	String testStr;
	
	CustomPhysicsSimulation(this.initPosition, this.velocity){}
	
	@override
	bool isDone(double time) {
		return false;
	}
	
	/// Since this velocity never changes, it will never slow down or speed up, maintains same speed
	@override
	double dx(double time) {
		print("VELOCITY == " + velocity.toString());
		return velocity;
	}
	
	@override
	double x(double time) {
		//Position scales up linearly as we scroll up
		var max = math.max(math.min(initPosition, 0.0), (initPosition + velocity * time));
		return max;
	}
	
	
}