import 'package:flutter/material.dart';

/// From: https://www.youtube.com/watch?v=vgcv4Fn9ERo&list=PLTAtF_0PvkNRd1OBo4n4a7Ct3bJoV34np&index=3
/// Simple Pager view with images + gradient
class PhotoPageView extends StatelessWidget {
	@override
	Widget build(BuildContext context) {
		return MaterialApp(
			title: "Gallery Demo",
			theme: ThemeData(primarySwatch: Colors.lightGreen),
			home: DisplayPage(),
		);
	}
	
	void printImageStr(String imageLoc){
		print("ImageLoc == " + imageLoc);
	}
}

class DisplayPage extends StatelessWidget {
	
	int _indexPosition;
	String _imageLocation;
	
	final List<String> images = [
		"assets/sample1.jpg",
		"assets/sample2.jpg",
		"assets/sample3.jpg"
	];
	
	@override
	Widget build(BuildContext context) {
		return Scaffold(
			
			body: Center(
				child: SizedBox.fromSize(
					size: Size.fromHeight(550.0),
					//Page view makes it so thing being viewed matches size of viewport
					//Can also access page controller, which controls which is being viewed (like a recylerview adapter)
					child: new InkWell(
						onTap: () => print("pressed the image"),
						child: PageView.builder(
							//@1 this will take up the whole space, at 0.75, 25% of the screen will have the other sides (pager padding)
							controller: PageController(viewportFraction: 0.8),
							itemCount: images.length,
							itemBuilder: (BuildContext context, int index){
								return new Padding(
									padding: EdgeInsets.symmetric(
										vertical: 16.0,
										horizontal: 8.0
									),
									child: Material(
										elevation: 5.0,
										borderRadius: BorderRadius.circular(8.0),
										child: Stack(
											
											fit: StackFit.expand, //Determines how non-positioned widget will fit into stack widget
											children: <Widget>[
												Image.asset(
													images[index],
													fit: BoxFit.cover //Cover the entire area
												),
												//Kinda like a padding widget, based on specific property
												DecoratedBox( //Adding in decorations: gradient
													decoration: BoxDecoration(
														gradient: LinearGradient(
//														begin: FractionalOffset.bottomRight,
//														end: FractionalOffset.topLeft,
															begin: FractionalOffset.bottomCenter,
															end: FractionalOffset.topCenter,
															colors: [  //Looking for list of colors in Hex
																Color(0x00000000).withOpacity(0.7), //70% opacity
																Color(0xff000000).withOpacity(0.01) //1% opacity
															]
														)
													),
												)
											],
										),
									),
								);
							},
						),
					)
				),
			),
		);
	}
}