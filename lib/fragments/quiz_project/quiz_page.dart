import 'package:flutter/material.dart';
import 'package:startup_namer/misc/Constants.dart';
import 'package:startup_namer/ui/question_text.dart';
import 'package:startup_namer/datamodels/question.dart';
import 'package:startup_namer/datamodels/quiz.dart';
import 'package:startup_namer/ui/answer_button.dart';
import 'package:startup_namer/ui/correct_wrong_overlay.dart';
import 'package:startup_namer/fragments/quiz_project/score_page.dart';

//Stateful widgets are immutable
//This is kind of like a holder view for the QuizPageState
/// From: https://www.youtube.com/watch?v=jBBl1tYkUnE&list=PLTAtF_0PvkNRd1OBo4n4a7Ct3bJoV34np&index=1
/// Quiz page, will move to the score page when all are done
class QuizPage extends StatefulWidget {
	@override
	State createState() => new QuizPageState();
}

//States are mutable
class QuizPageState extends State<QuizPage> {
	
	static List<Question> quizQuestions = [
		new Question("5 * 5 = 25", true),
		new Question("4 + 2 = 6", true),
		new Question("4 + 2 = 7", false),
		new Question("Computers don't use energy", false),
		new Question("A is after B in the alphabet", false),
	];
	
	Question currentQuestion;
	Quiz quiz = new Quiz(quizQuestions);
	
	//Locals
	String questionText;
	int questionNumber;
	bool isCorrect, overlayShouldBeVisible = false;
	
	
	@override
	void initState() {
		super.initState();
		this.currentQuestion = this.quiz.nextQuestion;
		this.questionText = this.currentQuestion.aQuestion;
		this.questionNumber = this.quiz.questionNumber;
	}
	
	@override
	Widget build(BuildContext context) {
		return new Stack( //Stack layers children on top of each other as opposed to a Column
			fit: StackFit.expand,
			children: <Widget>[
				//Container for tons of stuff (Main page essentially)
				new Column(
					children: <Widget>[
						//Order does matter here
						new AnswerButton(true,  () => userAnsweredQuestion(true)),
						new QuestionText(this.questionText, this.questionNumber),
						new AnswerButton(false, () => userAnsweredQuestion(false)),
					], //<Widget>[]
				), //Column
				(this.overlayShouldBeVisible) ? new CorrectWrongOverlay(
					this.isCorrect, (){
						if(this.questionNumber >= this.quiz.numQuestions){
							navigateToScorePage(context);
						} else {
							this.currentQuestion = this.quiz.nextQuestion;
							this.setState((){
								this.overlayShouldBeVisible = false;
								this.questionText = this.currentQuestion.aQuestion;
								this.questionNumber = this.quiz.questionNumber;
							});
						}
				}) : new Container()
			] //<Widget>[]
		); //Stack
	}
	
	void navigateToScorePage(BuildContext context){
		///This version does the standard add and go
//		Navigator
//			.of(context)
//			.push(new MaterialPageRoute(
//			builder: (BuildContext context) => new ScorePage(
//				this.quiz.score, this.quiz.numQuestions
//				)
//			)
//		);
		///This one below will clear the backstack
		Navigator
			.of(context)
			.pushAndRemoveUntil(new MaterialPageRoute(
			builder: (BuildContext context) => new ScorePage(
				this.quiz.score, this.quiz.numQuestions)
			),
			(Route route) => route == null
		);
	}
	

	
	/// Handles the response from user input
	void handleAnswer(bool answer){
		this.isCorrect = (this.currentQuestion.answer == answer);
		this.quiz.answerQuestion(this.isCorrect);
		this.setState((){
			//Here is where we display the overlay as per the answer
			this.overlayShouldBeVisible = true;
		});
	}
	
	void userAnsweredQuestion(bool isTrue){
		if(isTrue){
			print("it is true, huzzah!");
		} else {
			print("it is false, :(");
		}
		handleAnswer(isTrue);
	}
	
}



