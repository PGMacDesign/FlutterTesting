import 'package:flutter/material.dart';
import 'package:startup_namer/fragments/quiz_project/quiz_page.dart';
/// From: https://www.youtube.com/watch?v=jBBl1tYkUnE&list=PLTAtF_0PvkNRd1OBo4n4a7Ct3bJoV34np&index=1
/// Landing page to being the quiz and move to the next section
class LandingPage extends StatelessWidget {


    @override
    Widget build(BuildContext context) {
        ///Material widget acts like a piece of paper
        return new Material(
            color: Colors.greenAccent,
            
            child: new InkWell( // Fancy button with an ripple animation and press state changes
                onTap: () => navigateToQuizPage(context),
                child: new Column(
                    mainAxisAlignment: MainAxisAlignment.center, //Center on Y axis as it is a column
                    children: <Widget>[
                        //First widget to display
                        new Text("Let's take a quiz yo", style: new TextStyle(
                            color: Colors.white,
                            fontSize: 50.0,
                            fontWeight: FontWeight.bold
                            )
                        ),
                        new Text("Tap to start!", style: new TextStyle(
                            color: Colors.white, //white10 is very transparent
                            fontSize: 20.0,
                            fontWeight: FontWeight.bold
                            )
                        )
                    ],
                ),
            ),
        );
    }
    
    void navigateToQuizPage(BuildContext context){
        Navigator
            .of(context)
            .push(new MaterialPageRoute(
            builder: (BuildContext context) => new QuizPage())
        );
    }
}