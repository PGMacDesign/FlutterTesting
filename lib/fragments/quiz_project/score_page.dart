import 'package:flutter/material.dart';
import 'package:startup_namer/fragments/quiz_project/landing_page.dart';

/// From: https://www.youtube.com/watch?v=jBBl1tYkUnE&list=PLTAtF_0PvkNRd1OBo4n4a7Ct3bJoV34np&index=1
/// Score page, shown after all quiz questions are completed and will loop back to the landing page when done
class ScorePage extends StatelessWidget {
	
	final int score;
	final int numQuestions;
	
	ScorePage(this.score, this.numQuestions){}
	
	@override
	Widget build(BuildContext context) {
//		super.build(context);
		return new Material(
			color: Colors.blue,
			child: new Column(
				mainAxisAlignment: MainAxisAlignment.center,
				children: <Widget>[
					new Text("Your Score: ", style: new TextStyle(
						color: Colors.white,
						fontWeight: FontWeight.bold,
						fontSize: 50.0
					)),
					new Text(this.score.toString() + "/" + this.numQuestions.toString(), style: new TextStyle(
						color: Colors.white,
						fontWeight: FontWeight.bold,
						fontSize: 50.0
					)),
					new IconButton(
						icon: new Icon(Icons.arrow_right),
						color: Colors.white,
						iconSize: 90.0,
						onPressed: () => navigateToLandingPage(context),
					)
				],
			)
		);
	}
	
	void navigateToLandingPage(BuildContext context){
		///This version does the standard add and go
//		Navigator
//			.of(context)
//			.push(new MaterialPageRoute(
//			builder: (BuildContext context) => new ScorePage(
//				this.quiz.score, this.quiz.numQuestions
//				)
//			)
//		);
		///This one below will clear the backstack
		Navigator
			.of(context)
			.pushAndRemoveUntil(new MaterialPageRoute(builder:
			(BuildContext context) => new LandingPage()),
				(Route route) => route == null);
	}
}

