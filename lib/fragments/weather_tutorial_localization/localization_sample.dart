import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'dart:async';

/// Build code from: https://www.youtube.com/watch?v=IhsHGJEOSYM&list=PLTAtF_0PvkNRd1OBo4n4a7Ct3bJoV34np&index=11
/// (These commands are to be ran in terminal)
/// Command for building locale files:
///         "flutter pub pub run intl_translation:extract_to_arb --output-dir={PATH_CHOSEN} lib\fragments\weather_tutorial_localization\localization_sample.dart;"
/// Command for building locale files with the intl_** arb files included:
///         "flutter pub pub run intl_translation:generate_from_arb --output-dir={PATH_CHOSEN} --no-use-deferred-loading lib/fragments/weather_tutorial_localization/intl_languages/intl_en.arb lib/fragments/weather_tutorial_localization/intl_languages/intl_es.arb lib/fragments/weather_tutorial_localization/intl_languages/intl_ja.arb lib\fragments\weather_tutorial_localization\localization_sample.dart"
class LocalizationSample extends StatelessWidget{
	
	@override
	Widget build(BuildContext context) {
		return MaterialApp(
			title: 'Some title here',
			theme: ThemeData(
				primarySwatch: Colors.blue
			),
			home: MaterialApp(
				key: key
			),
		);
	}
}

class SampleLocalizationStuff1{
	static Future<SampleLocalizationStuff1> load(Locale locale){
		final String name = locale.countryCode.isEmpty ? locale.languageCode : locale.toString();
		final String localName = Intl.canonicalizedLocale(name);
//		return initializeMessages(localName).then((bool){ //Callback function on the same
//			Intl.defaultLocale = localName;
//			return SampleLocalizationStuff1();
//		});
		return null;
	}
	
	/// Allows it to be accessed anywhere
	static SampleLocalizationStuff1 of(BuildContext context){
		return Localizations.of<SampleLocalizationStuff1>(context, SampleLocalizationStuff1);
	}
	
	String get title {
		return Intl.message(
			"Some application",
			name: "title",
			desc: "The title of the app"
		);
	}
	
	String get buttonText {
		return Intl.message(
			"Click Me",
			name: "buttonText",
			desc: "The text on the button"
		);
	}
}

class SampleLocalizationDelegate extends LocalizationsDelegate<SampleLocalizationStuff1> {
	const SampleLocalizationDelegate();
	
	@override
	bool isSupported(Locale locale) {
		/// Supported language codes go here
		return ["en", "es", "ja"].contains(locale.languageCode);
	}
	
	@override
	bool shouldReload(LocalizationsDelegate<SampleLocalizationStuff1> old) {
		/// Asks whether or not the app should reload everything, which we are only replacing strings here.
		return false;
	}
	
	@override
	Future<SampleLocalizationStuff1> load(Locale locale) {
		return SampleLocalizationStuff1.load(locale);
	}
	
	
}