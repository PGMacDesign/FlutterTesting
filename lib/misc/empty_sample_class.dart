import 'package:flutter/material.dart';

class NameOfClassHere extends StatelessWidget{
	
	@override
	Widget build(BuildContext context) {
		return MaterialApp(
			title: 'Some title here',
			theme: ThemeData(
				primarySwatch: Colors.blue
			),
			home: SubNameOfAppHere(),
		);
	}
}

class SubNameOfAppHere extends StatefulWidget {
	@override
	AppStateNameHere createState() => AppStateNameHere();
}

class AppStateNameHere extends State<SubNameOfAppHere> {
	
	@override
	Widget build(BuildContext context) {
		// do stuff here
		return null;
	}
}