package misc;

/**
 * Created by pmacdowell on 6/20/2018.
 */

public class WebLinks {

    /**
     * Link to a nice web gui UI editor
     */
    public static final String FLUTTER_WEB_GUI_EDITOR = "https://flutterstudio.app";
}
