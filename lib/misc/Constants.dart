import 'package:flutter/material.dart';

class Constants {
	// Consts fields are completely immutable where a final object can be changed. Src:
	// https://news.dartlang.org/2012/06/const-static-final-oh-my.html
	static const double MAX_FONT_SIZE_ON_ANIMATIONS = 25.0;
	static const double SIZE_OF_CHECKMARK_ICON = 80.0;
	static const int TIME_IN_MILLISECONDS_ANIMATION_LOAD = 800;
	static const BOX_COLOR = Colors.cyan;
	
}