import 'package:flutter/material.dart';
import '../misc/Constants.dart';
class QuestionText extends StatefulWidget {
	
	final String _questionTextDisplay;
	final int _questionNumber;
	
	QuestionText(this._questionTextDisplay, this._questionNumber){}
	
	@override
	State createState() => new QuestionTextState();
}

class QuestionTextState extends State<QuestionText> with SingleTickerProviderStateMixin{ //Need a ticket provider state when using animations
	
	Animation<double> _fontSizeAnimation;
	AnimationController _fontSizeAnimationController;
	
	
	
	@override
	void initState() {
		super.initState(); //To pass to parent
		this._fontSizeAnimationController = new AnimationController(
			duration: new Duration(milliseconds: Constants.TIME_IN_MILLISECONDS_ANIMATION_LOAD),
			vsync: this //This will handle animations so they don't jump out of the screen (amongst other things)
		);
		this._fontSizeAnimation = new CurvedAnimation(
			parent: _fontSizeAnimationController,
			curve: Curves.bounceInOut
		);
		this._fontSizeAnimation.addListener(() => this.setState((){})); //Required format to make it re-render text in this context
		this._fontSizeAnimationController.forward();
	}
	
	
	@override
	void dispose() {
		this._fontSizeAnimationController.dispose(); //Free the resources before calling dispose
		super.dispose();
	}
	
	///Every time widget updates, this compares it to the old one
	@override
	void didUpdateWidget(QuestionText oldWidget) {
		super.didUpdateWidget(oldWidget);
		if(oldWidget._questionTextDisplay != widget._questionTextDisplay){
			_fontSizeAnimationController.reset();
			_fontSizeAnimationController.forward();
		}
	}
	
	@override
	Widget build(BuildContext context) {
		return new Material(
			color:Colors.white,
			child: new Padding(
				padding: new EdgeInsets.symmetric(vertical: 15.0, horizontal: 0.1),
				child: new Center(
					child: new Text(buildQuestionText(
							widget._questionNumber, widget._questionTextDisplay),
						style: new TextStyle(
							fontSize: (Constants.MAX_FONT_SIZE_ON_ANIMATIONS * _fontSizeAnimation.value),
							color: Colors.blueGrey
						)
					),
				),
			)
		);
	}
}

String buildQuestionText(int quesNum, String text){
	return ("Question " + quesNum.toString() + ": " + text);
}