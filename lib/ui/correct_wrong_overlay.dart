import 'package:flutter/material.dart';
import 'package:startup_namer/misc/Constants.dart';
import 'dart:math';
class CorrectWrongOverlay extends StatefulWidget {
	
	final bool _isCorrect;
	final VoidCallback _onTopCallback;
	
	CorrectWrongOverlay(this._isCorrect, this._onTopCallback){}
	
	@override
	State createState() => new CorrectWrongOverlayState();
}

class CorrectWrongOverlayState extends State<CorrectWrongOverlay> with SingleTickerProviderStateMixin{
	
	Animation<double> _iconAnimation;
	AnimationController _iconAnimationController;
	
	@override
	void initState() {
		super.initState();
		this._iconAnimationController = new AnimationController(vsync: this,
			duration: new Duration(milliseconds: Constants.TIME_IN_MILLISECONDS_ANIMATION_LOAD));
		this._iconAnimation = new CurvedAnimation(parent: _iconAnimationController, curve: Curves.elasticOut);
		this._iconAnimation.addListener(() => this.setState((){})); //Empty constructor here
		this._iconAnimationController.forward(); //Empty params on purpose
	}
	
	
	@override
	void dispose() {
		this._iconAnimationController.dispose(); //Free the resources before calling dispose
		super.dispose();
	}
	
	@override
	Widget build(BuildContext context) {
		return new Material(
			color: Colors.black54, //54% opacity
			child: new InkWell(
				onTap: () => widget._onTopCallback(),
				child: new Column(
					mainAxisAlignment: MainAxisAlignment.center,
					children: <Widget>[
						new Container(
							//Create icon here
							//child: new Icon(buildRightWrongIcon(widget._isCorrect),
								//size: Constants.SIZE_OF_CHECKMARK_ICON,),
							child: new Transform.rotate(
								angle: (this._iconAnimation.value * 2 * pi),
								child: new Icon(buildRightWrongIcon(widget._isCorrect),
									size: Constants.SIZE_OF_CHECKMARK_ICON * this._iconAnimation.value)
							),
							decoration: new BoxDecoration(
								color: Colors.white,
								shape: BoxShape.circle,
								
							),
						),
						new Padding(padding: new EdgeInsets.only(bottom: 20.0)),
						new Text(buildRightWrongText(widget._isCorrect), style: new TextStyle(
								color: Colors.white,
								fontSize: 30.0,
							),
						)
					],
				),
			),
		);
	}
}

IconData buildRightWrongIcon(bool isCorrect){
	return (isCorrect) ? Icons.done : Icons.clear;
}

String buildRightWrongText(bool isCorrect){
	return (isCorrect) ? "Correct. Congrats!" : "Wrong. /sadPanda";
}