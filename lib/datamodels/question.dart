
class Question {
	final String aQuestion;
	final bool answer;
	
	//New way for constructor
	Question(this.aQuestion, this.answer);
	
//	//Old way for constructor
//	Question(String question, bool answer){
//		this.answer = answer;
//		this.question = question;
//	}


}