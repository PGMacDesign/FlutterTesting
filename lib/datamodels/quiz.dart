import 'package:startup_namer/datamodels/question.dart';


class Quiz {
	
	List<Question> _questions;
	int _currentQuestionIndex = -1;
	int _score = 0;
	
	Quiz(this._questions){
		this._questions.shuffle();
	}
	
	int get score => _score;
	
	int get numQuestions => _questions.length;
	
	int get questionNumber => _currentQuestionIndex+1;
	
	List<Question> get questions => _questions;
	
	Question get nextQuestion{
		_currentQuestionIndex++;
		if(_currentQuestionIndex < 0 || _currentQuestionIndex >= numQuestions){
			return null;
		}
		return questions[_currentQuestionIndex];
	}
	
	void answerQuestion(bool isCorrect) {
		if(isCorrect){
			_score++;
		}
	}
	
}