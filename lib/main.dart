import 'package:flutter/material.dart';
import 'package:startup_namer/fragments/misctutorials/app_lifecycle_screen_orientation.dart';
import 'package:startup_namer/fragments/misctutorials/drag_and_drop_sample.dart';
import 'package:startup_namer/fragments/misctutorials/gestures_and_detectors.dart';
import 'package:startup_namer/fragments/misctutorials/photo_page_view.dart';
import 'package:startup_namer/fragments/misctutorials/scoped_model_pattern.dart';
import 'package:startup_namer/fragments/misctutorials/shared_preferences_flex_widgets_dismissibles.dart';
import 'package:startup_namer/fragments/misctutorials/spring_box.dart';
import 'package:startup_namer/fragments/quiz_project/landing_page.dart';
import 'package:startup_namer/fragments/misctutorials/sliver_dynamic_layout.dart';

/// Starting op
void main(){
    runApp(new MaterialApp(
        debugShowCheckedModeBanner: false,
        debugShowMaterialGrid: false,
//        home: new LandingPage() // This will run the Test app with landing page + quiz + score result page
//        home: new SpringBox() // This will test the spring box physics engine with stretching box
//        home: new PhotoPageView() //This will test the photo gallery pager adapter
//        home: new SliverDynamicLayout() //This will test the slivers layout
//        home: AppLifecycleScreenOrientation() //This will show the app lifecycle orientation layout
//        home: DragAndDropSample() //This will show the drag and drop layout sample
//        home: ManageScopedModelPattern() //Demoing the Scope Model pattern samples
//        home: InheritedWidgetsAndGestureDetectors() //Gesture detectors samples
        home: SharedPreferencesFlexWidgetsDismissibles()
    ));
    
    
    
}